<?php

add_action( 'wp_enqueue_scripts', 'add_child_theme_stylesheets', PHP_INT_MAX );
function add_child_theme_stylesheets() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
 	wp_enqueue_style( 'hicaliber-child-theme', get_stylesheet_directory_uri() . '/assets/css/hicaliber-child-theme.css' );
 	wp_enqueue_script( 'hicaliber-child-js', get_stylesheet_directory_uri() . '/assets/js/hicaliber-child-script.js', array('jquery'), '1.0', true );
}

add_action("wp_footer", function(){
	?>
	<script defer>
		(function($){
			$(document).ready(function(){
				$('#input_9_1').on('input',function(e){

		        $("#company_name").text( $('#input_9_1').val() );
		    });
			});
		})(jQuery);
	</script>
	<?php
}, PHP_INT_MAX);
