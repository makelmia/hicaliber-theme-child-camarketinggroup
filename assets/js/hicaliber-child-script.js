jQuery(document).ready(function($) {
   //Add Your Custom Script here

   $('.published-date').html(function(i,ab) {
      return ab.replace(/\d+/g, function(v){
        return "<div class='numbers'>" + v + "</div>";
      });
    });

    $('#departments.carousel .section-body').slick({
        slidesToShow: 8,
        slidesToScroll: 8,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 8,
              slidesToScroll: 8,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
    });
});
